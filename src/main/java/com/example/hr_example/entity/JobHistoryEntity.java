package com.example.hr_example.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class JobHistoryEntity {
    private Integer employeeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer jobId;
    private Integer departmentId;
}
