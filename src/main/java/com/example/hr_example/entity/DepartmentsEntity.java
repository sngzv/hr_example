package com.example.hr_example.entity;

import com.example.hr_example.dto.LocationsDto;
import lombok.Data;

@Data
public class DepartmentsEntity {
    private Integer departmentId;
    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
