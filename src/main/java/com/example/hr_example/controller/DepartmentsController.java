package com.example.hr_example.controller;

import com.example.hr_example.dto.DepartmentsDTO;
import com.example.hr_example.service.api.DepartmentsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/departments")
@AllArgsConstructor
public class DepartmentsController {

    private final DepartmentsService departmentsService;

    @Operation(summary = "Получение департамента по id")
    @GetMapping("/{id}")
    public Map getDepartmentById(@PathVariable Integer id) {
        return departmentsService.getDepartmentById(id);
    }

    @Operation(summary = "Добавление нового департамента")
    @PostMapping
    public void insertDepartment(@RequestBody DepartmentsDTO dto) {
        departmentsService.insertDepartment(dto);
    }

    @Operation(summary = "Обновление департамента по id")
    @PutMapping
    public void updateDepartment(@RequestBody DepartmentsDTO dto) {
        departmentsService.updateDepartment(dto);
    }

    @Operation(summary = "Фильтрация департамента по его названию")
    @GetMapping("/filterDepartmentsFilter")
    public List<DepartmentsDTO> getFilterDepartmenstNameAll() {
        return departmentsService.getFilterDepartmensNameAll();
    }

    @Operation(summary = "Фильтрация департамента по фамилии руководителя")
    @GetMapping("/getFilterDepartmenstLastNameChief")
    public List<Map> getFilterDepartmenstLastNameChief() {
        return departmentsService.getFilterDepartmenstLastNameChief();
    }
}
