package com.example.hr_example.controller;

import com.example.hr_example.dto.LocationsDto;
import com.example.hr_example.service.api.LocationsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
@RequiredArgsConstructor
public class LocationsController {

    private final LocationsService locationsService;

    @Operation(summary = "Получение всех локаций")
    @GetMapping
    public List<LocationsDto> getAll() {
        return locationsService.getAll();
    }

    @Operation(summary = "Получение локации по id")
    @GetMapping("/{locationId}")
    public LocationsDto getLocationById(@PathVariable Integer locationId) {
        return locationsService.getLocationById(locationId);
    }

    @Operation(summary = "Добавление новой локации")
    @PostMapping
    public LocationsDto insertLocation(@RequestBody LocationsDto dto) {
        return locationsService.insertLocation(dto);
    }
}
