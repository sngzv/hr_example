package com.example.hr_example.controller;

import com.example.hr_example.dto.EmployeesDTO;
import com.example.hr_example.dto.JobsDTO;
import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.service.api.EmployeesService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/employees")
public class EmployeesController {

    private final EmployeesService employeesService;

    @Operation(summary = "Получение всех сотрудников")
    @GetMapping
    public List<EmployeesDTO> getAll() {
        return employeesService.getAll();
    }

    @Operation(summary = "Получение сотрудника по ключу(фамилии, имени, email)")
    @GetMapping("/search/{name}")
    public List<EmployeesDTO> searchName(@PathVariable String name) {
        return employeesService.search(name);
    }

    @Operation(summary = "Получение сотрудника по id")
    @GetMapping("/{id}")
    public List<Map> getEmploymentById(@PathVariable Integer id) {
        return employeesService.getEmployeeById(id);
    }

    @Operation(summary = "Добавление нового сотрудника")
    @PostMapping
    public void insertEmployee(@RequestBody EmployeesDTO dto) {
        employeesService.insertEmployee(dto);
    }

    @Operation(summary = "Добавление нового сотрудника с работой")
    @PostMapping("/addEmployeeAndJob")
    public void insertEmployeeAndJob(@RequestBody EmployeesDTO employeesDTO, @RequestBody JobsDTO jobsDTO) {
        employeesService.insertEmployeeAndJob(employeesDTO, jobsDTO);
    }

    @Operation(summary = "Обновление сотрудника по id")
    @PutMapping
    public void updateEmployee(@RequestBody EmployeesDTO dto) {
        employeesService.updateEmployee(dto);
    }
}
