package com.example.hr_example.dto;

import lombok.Data;

@Data
public class DepartmentsDTO {
    private Integer departmentId;
    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
