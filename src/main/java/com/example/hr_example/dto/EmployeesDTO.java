package com.example.hr_example.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class EmployeesDTO {
    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private LocalDate hireDate;
    private Integer jobId;
    private Integer salary;
    private Integer managerId;
    private Integer departmentId;
}
