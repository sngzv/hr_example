package com.example.hr_example.dto;

import lombok.Data;

@Data
public class LocationsDto {
    private Integer locationId;
    private String streetAddress;
    private String city;
}
