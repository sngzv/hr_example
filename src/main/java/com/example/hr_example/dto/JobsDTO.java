package com.example.hr_example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class JobsDTO {
    private String jobId;
    private String jobTitle;
    private Integer minSalary;
    private Integer maxSalary;
}
