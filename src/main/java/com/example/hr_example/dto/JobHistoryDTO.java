package com.example.hr_example.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class JobHistoryDTO {
    private Integer employeeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer jobId;
    private Integer departmentId;
}
