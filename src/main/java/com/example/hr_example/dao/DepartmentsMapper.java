package com.example.hr_example.dao;

import com.example.hr_example.entity.DepartmentsEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DepartmentsMapper {

    @Select("select distinct on (d.department_id) d.department_id, d.department_name, e.first_name, e.last_name, l.street_address, l.city \n" +
            "from departments d\n" +
            "left join locations l\n" +
            "on l.location_id = d.location_id\n" +
            "left join employees e on d.department_id = e.department_id\n" +
            "where d.department_id = #{id}")
    @ResultType(value = Map.class)
    Map getDepartmentById(@Param("id") Integer id);

    @Insert("insert into departments(department_id, department_name, manager_id, location_id) " +
            "values (#{departmentId}, #{departmentName}, #{managerId}, #{locationId})")
    @SelectKey(keyProperty = "departmentId", before = true, resultType = Integer.class,
            statement = "select nextval('departments_seq')")
    void insertDepartment(DepartmentsEntity entity);

    @Update("update departments set department_name = #{departmentsEntity.departmentName}, manager_id = #{departmentsEntity.managerId}, location_id = #{departmentsEntity.locationId} " +
            "where department_id = #{id}")
    void updateDepartment(Integer id,  DepartmentsEntity departmentsEntity);

    @Select("select * from departments order by department_name")
    List<DepartmentsEntity> getFilterDepartmenstNameAll();

    @Select("select distinct d.department_id, d.department_name, e.first_name, e.last_name\n" +
            "from departments d\n" +
            "left join employees e on d.department_id = e.department_id\n" +
            "order by e.last_name")
    List<Map> getFilterDepartmenstLastNameChief();

}
