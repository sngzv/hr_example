package com.example.hr_example.dao;

import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.entity.JobHistoryEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface JobHistoryMapper {

    @Select("select * from job_history where employee_id = #{employeeId}")
    JobHistoryEntity getJobHistoryById(Integer id);

    @Update("update job_history set job_id = #{entity.jobId}, department_id = #{entity.departmentId} where employee_id = #{id}")
    void updateJobHistory(Integer id, JobHistoryEntity entity);

    @Insert("insert into job_history(employee_id, start_date, end_date, job_id, department_id) VALUES (\n" +
            "#{employeeId}, #{hireDate}, current_date, #{jobId}, #{departmentId}\n" +
            ")")
    void insertJobHistory(EmployeesEntity entity);
}
