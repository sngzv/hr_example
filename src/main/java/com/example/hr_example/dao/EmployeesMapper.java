package com.example.hr_example.dao;

import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.entity.JobEntity;
import com.example.hr_example.entity.JobHistoryEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface EmployeesMapper {

    @Select("select * from employees")
    List<EmployeesEntity> getAll();

    @Select("select * from employees where first_name = #{name}")
    List<EmployeesEntity> searchName(@Param("name") String name);
    @Select("select * from employees where last_name = #{name}")
    List<EmployeesEntity> searchSurname(@Param("name") String name);
    @Select("select * from employees where email = #{name}")
    List<EmployeesEntity> searchEmail(@Param("name") String name);
    @Select("select * from employees where hire_date = #{name}")
    List<EmployeesEntity> searchDate(@Param("name") String name);

    @Select("select * from employees e\n" +
            "left join departments d on d.department_id = e.department_id\n" +
            "left join job_history jh on e.employee_id = jh.employee_id where e.employee_id = #{id}")
    List<Map> getEmployeeById(Integer id);

    @Insert("insert into employees(employee_id, first_name, last_name,\n" +
            "                      email, phone_number, hire_date,\n" +
            "                      job_id, salary, manager_id, department_id)\n" +
            "VALUES (#{employeeId}, #{firstName}, #{lastName}, #{email}, #{phoneNumber}," +
            " #{hireDate}, #{jobId}, #{salary}, #{managerId}, #{departmentId});")
    @SelectKey(keyProperty = "employeeId", before = true, resultType = Integer.class,
            statement = "select nextval('employees_seq')")
    void insertEmployee(EmployeesEntity employeeEntity);

    @Insert("insert into jobs(job_id, job_title, min_salary, max_salary) VALUES (#{jobId},#{jobTitle},#{minSalary},#{maxSalary})")
    void insertJob(JobEntity entity);

    @Update("update employees " +
            "set first_name = #{entity.firstName}, " +
            "last_name = #{entity.lastName}, " +
            "email = #{entity.email}, " +
            "phone_number = #{entity.phoneNumber}, " +
            "hire_date = #{entity.hireDate}, " +
            "job_id = #{entity.jobId}, " +
            "salary = #{entity.salary}, " +
            "manager_id = #{entity.managerId}, " +
            "department_id = #{entity.departmentId} where employee_id = #{id}")
    void updateEmployee(Integer id, EmployeesEntity entity);
}
