package com.example.hr_example.dao;

import com.example.hr_example.entity.LocationsEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface LocationsMapper {

    @Select("select * from locations where location_id = #{id} ")
    Optional<LocationsEntity> getLocationById(Integer id);

    @Select("select * from locations")
    List<LocationsEntity> getAll();

    @Insert("insert into locations(location_id, street_address, city) VALUES " +
            "(#{locationId}, #{streetAddress}, #{city})")
    @SelectKey(keyProperty = "locationId", before = true, resultType = Integer.class,
            statement = "select nextval('locations_seq')")
    void insert(LocationsEntity entity);
}
