package com.example.hr_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrExampleApplication.class, args);
    }

}
