package com.example.hr_example.service.api;

import com.example.hr_example.dto.DepartmentsDTO;

import java.util.List;
import java.util.Map;

public interface DepartmentsService {

    Map getDepartmentById(Integer id);

    void insertDepartment(DepartmentsDTO dto);

    void updateDepartment(DepartmentsDTO dto);

    List<DepartmentsDTO> getFilterDepartmensNameAll();

    List<Map> getFilterDepartmenstLastNameChief();

}
