package com.example.hr_example.service.api;

import com.example.hr_example.dto.EmployeesDTO;
import com.example.hr_example.dto.JobHistoryDTO;
import com.example.hr_example.entity.EmployeesEntity;

public interface JobHistoryService {

    JobHistoryDTO getJobHistoryById(Integer id);

    void insertJobHistory(EmployeesDTO dto);
}
