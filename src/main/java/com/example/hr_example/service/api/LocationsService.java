package com.example.hr_example.service.api;

import com.example.hr_example.dto.LocationsDto;

import java.util.List;

public interface LocationsService {

    List<LocationsDto> getAll();

    LocationsDto getLocationById(Integer id);

    LocationsDto insertLocation(LocationsDto dto);
}
