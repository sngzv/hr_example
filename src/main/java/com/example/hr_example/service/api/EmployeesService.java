package com.example.hr_example.service.api;

import com.example.hr_example.dto.EmployeesDTO;
import com.example.hr_example.dto.JobsDTO;
import com.example.hr_example.dto.JobHistoryDTO;

import java.util.List;
import java.util.Map;

public interface EmployeesService {

    List<EmployeesDTO> getAll();

    List<EmployeesDTO> search(String name);

    List<Map> getEmployeeById(Integer id);

    void insertEmployee(EmployeesDTO employeeEntity);

    void insertEmployeeAndJob(EmployeesDTO employeesDTO, JobsDTO jobsDTO);

    void updateEmployee(EmployeesDTO dto);
}
