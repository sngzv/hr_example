package com.example.hr_example.service.impl;

import com.example.hr_example.dao.DepartmentsMapper;
import com.example.hr_example.dto.DepartmentsDTO;
import com.example.hr_example.entity.DepartmentsEntity;
import com.example.hr_example.service.api.DepartmentsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class DepartmentsServiceImpl implements DepartmentsService {

    private final ModelMapper modelMapper;
    private final DepartmentsMapper departmentsMapper;

    @Override
    public Map getDepartmentById(Integer id) {
        log.debug("Получена сущность департамента с id={}", id);
        return departmentsMapper.getDepartmentById(id);
    }

    @Override
    public void insertDepartment(DepartmentsDTO dto) {
        DepartmentsEntity departmentsEntity = modelMapper.map(dto, DepartmentsEntity.class);
        departmentsMapper.insertDepartment(departmentsEntity);
        log.info("Создана сущность департамента с id={}", departmentsEntity.getDepartmentId());
    }

    @Override
    public void updateDepartment(DepartmentsDTO dto) {
        DepartmentsEntity entity = modelMapper.map(dto, DepartmentsEntity.class);
        departmentsMapper.updateDepartment(entity.getDepartmentId(), entity);
        log.info("Обновлена сущность департамента с id={}", entity.getLocationId());
    }

    @Override
    public List<DepartmentsDTO> getFilterDepartmensNameAll() {
        List<DepartmentsEntity> entities = departmentsMapper.getFilterDepartmenstNameAll();
        log.debug("Получены все сущности департаментов с фильтрацией по названию");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<Map> getFilterDepartmenstLastNameChief() {
        log.debug("Получениы все сущности департаментов с фильтрацией по фамилии главы");
        return departmentsMapper.getFilterDepartmenstLastNameChief();
    }

}
