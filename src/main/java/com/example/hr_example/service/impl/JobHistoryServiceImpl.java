package com.example.hr_example.service.impl;

import com.example.hr_example.dao.JobHistoryMapper;
import com.example.hr_example.dto.EmployeesDTO;
import com.example.hr_example.dto.JobHistoryDTO;
import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.entity.JobHistoryEntity;
import com.example.hr_example.service.api.JobHistoryService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JobHistoryServiceImpl implements JobHistoryService {

    private final ModelMapper modelMapper;
    private final JobHistoryMapper jobHistoryMapper;

    @Override
    public JobHistoryDTO getJobHistoryById(Integer id) {
        JobHistoryEntity entity = jobHistoryMapper.getJobHistoryById(id);
        return modelMapper.map(entity, JobHistoryDTO.class);
    }

    @Override
    public void insertJobHistory(EmployeesDTO dto) {
        EmployeesEntity entity = modelMapper.map(dto, EmployeesEntity.class);
        jobHistoryMapper.insertJobHistory(entity);
    }
}
