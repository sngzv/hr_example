package com.example.hr_example.service.impl;

import com.example.hr_example.dao.LocationsMapper;
import com.example.hr_example.dto.LocationsDto;
import com.example.hr_example.entity.LocationsEntity;
import com.example.hr_example.service.api.LocationsService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LocationsServiceImpl implements LocationsService {

    private final LocationsMapper locationsMapper;
    private final ModelMapper modelMapper;

    @Override
    public List<LocationsDto> getAll() {
        List<LocationsEntity> entities = locationsMapper.getAll();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    @Transactional(readOnly = true)
    public LocationsDto getLocationById(Integer id) {
        LocationsEntity locationsEntity = locationsMapper.getLocationById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность локации по идентификатору=%s", id)
                )
        );
        return modelMapper.map(locationsEntity, LocationsDto.class);
    }

    @Override
    @Transactional
    public LocationsDto insertLocation(LocationsDto dto) {
        LocationsEntity locationsEntity = modelMapper.map(dto, LocationsEntity.class);
        locationsMapper.insert(locationsEntity);
        return getLocationById(locationsEntity.getLocationId());
    }
}
