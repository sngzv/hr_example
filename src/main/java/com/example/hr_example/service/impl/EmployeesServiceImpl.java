package com.example.hr_example.service.impl;

import com.example.hr_example.dao.EmployeesMapper;
import com.example.hr_example.dao.JobHistoryMapper;
import com.example.hr_example.dto.EmployeesDTO;
import com.example.hr_example.dto.JobsDTO;
import com.example.hr_example.dto.JobHistoryDTO;
import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.entity.JobEntity;
import com.example.hr_example.entity.JobHistoryEntity;
import com.example.hr_example.service.api.EmployeesService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesMapper employeesMapper;
    private final ModelMapper modelMapper;
    private final JobHistoryMapper jobHistoryMapper;

    @Override
    public List<EmployeesDTO> getAll() {
        List<EmployeesEntity> entities = employeesMapper.getAll();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeesDTO> search(String name) {
        List<EmployeesEntity> entities = employeesMapper.searchName(name);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<Map> getEmployeeById(Integer id) {
        return employeesMapper.getEmployeeById(id);
    }

    @Override
    public void insertEmployee(EmployeesDTO dto) {
        EmployeesEntity entity = modelMapper.map(dto, EmployeesEntity.class);
        employeesMapper.insertEmployee(entity);
        JobHistoryDTO jobsHistoryDTO = new JobHistoryDTO();
        jobsHistoryDTO.setEmployeeId(entity.getEmployeeId());
        jobsHistoryDTO.setStartDate(entity.getHireDate());
        jobsHistoryDTO.setEndDate(LocalDate.now());
        jobsHistoryDTO.setJobId(entity.getJobId());
        jobsHistoryDTO.setDepartmentId(entity.getDepartmentId());
        insertJobHistory(jobsHistoryDTO);
    }

    @Override
    public void insertEmployeeAndJob(EmployeesDTO employeesDTO, JobsDTO jobsDTO) {
        EmployeesEntity employeesEntity = modelMapper.map(employeesDTO, EmployeesEntity.class);
        JobEntity jobEntity = modelMapper.map(jobsDTO, JobEntity.class);
        employeesMapper.insertEmployee(employeesEntity);
        employeesMapper.insertJob(jobEntity);
    }

    public void insertJobHistory(JobHistoryDTO dto) {
        JobHistoryEntity entity = modelMapper.map(dto, JobHistoryEntity.class);
        //employeesMapper.insertJobHistory(entity);
    }

    @Override
    public void updateEmployee(EmployeesDTO dto) {
        EmployeesEntity entityUp = modelMapper.map(dto, EmployeesEntity.class);
        Integer id = entityUp.getEmployeeId();
        JobHistoryEntity jobHistoryEntity = jobHistoryMapper.getJobHistoryById(id);
        if (jobHistoryEntity != null) {
            jobHistoryEntity.setStartDate(entityUp.getHireDate());
            jobHistoryEntity.setJobId(entityUp.getJobId());
            jobHistoryEntity.setDepartmentId(entityUp.getDepartmentId());
            jobHistoryMapper.updateJobHistory(id, jobHistoryEntity);
        } else {
            jobHistoryMapper.insertJobHistory(entityUp);
        }
        employeesMapper.updateEmployee(id, entityUp);
    }
}
